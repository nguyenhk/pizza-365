$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gPIZZE_SIZE = {
    small: {
      kichCo: "S",
      duongKinh: 20,
      suon: 2,
      salad: 200,
      soLuongNuoc: 2,
      thanhTien: 150000,
    },
    medium: {
      kichCo: "M",
      duongKinh: 25,
      suon: 4,
      salad: 300,
      soLuongNuoc: 3,
      thanhTien: 200000,
    },
    large: {
      kichCo: "L",
      duongKinh: 30,
      suon: 8,
      salad: 500,
      soLuongNuoc: 4,
      thanhTien: 250000,
    },
  };

  const gPIZZE_TYPE = {
    hawaii: {
      loaiPizza: "Hawaii",
    },
    seafood: {
      loaiPizza: "Seafood",
    },
    bacon: {
      loaiPizza: "Bacon",
    },
  };

  // bạn có thể dùng để lưu loại pizza đươc chọn, mỗi khi khách chọn, bạn lại đổi giá trị cho nó
  var gSelectedSize = {};
  var gSelectedType = {};
  var gSelectedDrink = {};
  var ObjectRequest = {};
  var gDISCOUNT = 0;

  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  onPageLoading();

  $(`button[id^='btn-size']`).on("click", function () {
    //reset màu btn
    changeColorBtnDefault("size");
    //đổi màu btn
    $(this).removeClass("btn-orange").addClass("btn-success");
    //gán combo vào biến toàn cục
    var vSizePizza = $(this).attr("name");
    gSelectedSize = gPIZZE_SIZE[vSizePizza];
    //in ra console
    console.log(`%cBạn đã chọn size: ${gSelectedSize.kichCo}`, "color:orange");
    console.log(gSelectedSize);
  });

  $(`button[id^='btn-type']`).on("click", function () {
    //reset màu btn
    changeColorBtnDefault("type");
    //đổi màu btn
    $(this).removeClass("btn-orange").addClass("btn-success");
    //gán combo vào biến toàn cục
    var vTypePizza = $(this).attr("name");
    gSelectedType = gPIZZE_TYPE[vTypePizza];
    //in ra console
    console.log(`%cBạn đã chọn loại pizza: ${gSelectedType.loaiPizza}`, "color:cyan");
  });

  $("#select-drinks").on("change", function () {
    var vDrinkValue = $(this).val();
    gSelectedDrink.idLoaiNuocUong = vDrinkValue;
    console.log(`%cBạn đã chọn lại nước: ${this.options[this.selectedIndex].text}`, "color:pink");
  });

  $("#btn-send-order").on("click", function () {
    // data để test
    // testDataInput();
    // lấy giá trị từ input
    var vObjectInfo = getValueFormInput("#form-info");
    // merge các object vs nhau
    ObjectRequest = { ...gSelectedSize, ...gSelectedType, ...gSelectedDrink, ...vObjectInfo };
    //valid data
    var isValid = validateCustomerData(ObjectRequest);
    if (isValid) {
      $("#confirm-order-modal").modal("show");
      importValueToFormInput("#confirm-order-modal", ObjectRequest);
      //tạo string ô detail
      var vStringInfo = `Xác nhận: ${ObjectRequest.hoTen}, ${ObjectRequest.soDienThoai}, ${ObjectRequest.diaChi}`;
      var vStringMenu = `Menu: ${ObjectRequest.kichCo} - Sườn nướng: ${ObjectRequest.suon} miếng, Salad: ${ObjectRequest.salad}g, Nước ngọt: ${ObjectRequest.soLuongNuoc}`;
      var vStringType = `Loại pizza: ${ObjectRequest.loaiPizza}`;
      var vStringTotal = `Giá ${ObjectRequest.thanhTien.toLocaleString("de-DE")}đ`;
      var vStringDiscount = "";
      if (!ObjectRequest.idVourcher) {
        vStringDiscount = `Bạn không sử dụng mã giảm giá. Số tiền thanh toán: ${ObjectRequest.thanhTien.toLocaleString(
          "de-DE"
        )}đ`;
      } else {
        vStringDiscount = `Mã giảm giá: ${ObjectRequest.idVourcher}. Số tiền thanh toán: ${(
          ObjectRequest.thanhTien - ObjectRequest.giamGia
        ).toLocaleString("de-DE")}đ`;
      }
      //gán giá trị
      $("#txt-detail").val(
        `${vStringInfo}\n${vStringMenu}\n${vStringType}\n${vStringTotal}\n${vStringDiscount}`
      );

      console.log(`%cThông tin đơn hàng của bạn: `, "color:purple");
      console.log(JSON.stringify(ObjectRequest));
    }
  });

  $("#btn-create-order").click(function () {
    $("#confirm-order-modal").modal("hide");
    createOrderObjectAPI(ObjectRequest);
  });

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

  function onPageLoading() {
    loadSelectDrinksData();
  }

  function importOptionToSelect(paramSelect, paramObject) {
    var keys = Object.keys(paramObject[0]);
    var vSelect = $(paramSelect);

    for (var i = 0; i < paramObject.length; i++) {
      $("<option>", {
        value: paramObject[i][keys[0]],
        text: paramObject[i][keys[1]],
      }).appendTo(vSelect);
    }
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //trả các button về trạng thái defaut
  function changeColorBtnDefault(paramBtn) {
    var vButtons = $(`button[id*=${paramBtn}]`);
    for (var vBtn of vButtons) {
      vBtn.className = "btn w-100 font-weight-bold btn-orange";
    }
  }

  function loadSelectDrinksData() {
    var vBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/drinks";
    var vOptionDrink = {};
    $.ajax({
      url: vBASE_URL,
      type: "GET",
      async: true,
      success: function (response) {
        vOptionDrink = response;
        importOptionToSelect("#select-drinks", vOptionDrink);
      },
      error: (err) => alert(err.status),
    });
  }

  function getValueFormInput(paramSelector) {
    var vInputElement = $(`${paramSelector} input[name]`);
    var vObject = {};
    for (var i = 0; i < vInputElement.length; i++) {
      vObject[vInputElement[i].name] = vInputElement[i].value;
    }
    return vObject;
  }

  function validateCustomerData(paramObject) {
    try {
      //kiểm tra đã chọn combo chưa
      if (!paramObject.kichCo) {
        throw "Vui lòng chọn combo";
      }
      if (!paramObject.loaiPizza) {
        throw "Vui lòng chọn loại pizza!!";
      }

      //Check đồ uống
      if (!paramObject.idLoaiNuocUong) {
        throw "Vui lòng chọn đồ uống!!";
      }
      if (paramObject.idLoaiNuocUong == "none") {
        throw "Vui lòng chọn đồ uống!!";
      }

      // check info khách hàng
      if (!paramObject.hoTen.trim()) {
        throw "Bạn chưa nhập họ và tên";
      }

      //Check email, email phải có @
      if (!paramObject.email.trim()) {
        throw "Bạn chưa nhập email";
      } else if (!validateEmail(paramObject.email)) {
        throw "Email không hợp lệ";
      }

      //Check số điện thoại
      if (!paramObject.soDienThoai) {
        throw "Bạn chưa nhập số điện thoại";
      } else if (!paramObject.soDienThoai.startsWith("0")) {
        throw "Số điện thoại không hợp lệ";
      } else if (!validatePhoneNumber(paramObject.soDienThoai)) {
        throw "Số điện thoại không hợp lệ";
      }

      //Check địa chỉ
      if (!paramObject.diaChi.trim()) {
        throw "Bạn chưa nhập địa chỉ";
      }
      //Check Voucher ID
      var vVouhcerId = paramObject.idVourcher;
      validateVoucherId(vVouhcerId);
    } catch (err) {
      alert(err);
      return false;
    }
    return true;
  }

  function validateEmail(paramEmail) {
    var re = /^\S+@\S+\.\S+$/;
    return re.test(paramEmail);
  }

  function validatePhoneNumber(paramPhoneNumber) {
    var re = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
    return re.test(paramPhoneNumber);
  }

  function validateVoucherId(paramVoucherId) {
    var vAlert = "";
    if (!paramVoucherId) {
      vAlert = `Bạn chưa nhập voucher`;
      console.log("Không có voucher");
    } else {
      const vBASE_URL = "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/";
      $.ajax({
        url: vBASE_URL + paramVoucherId,
        type: "GET",
        async: false,
        success: function (response) {
          gDISCOUNT = response.phanTramGiamGia;
          ObjectRequest.giamGia = (ObjectRequest.thanhTien * gDISCOUNT) / 100;
          vAlert = `Mã voucher hợp lệ!! Bạn được giảm ${gDISCOUNT}% trên giá thanh toán`;
          console.log("Mã voucher đúng");
        },
        error: function (err) {
          vAlert = "Mã voucher không hợp lệ";
          console.log("Mã voucher không đúng");
          ObjectRequest.idVourcher = "";
          ObjectRequest.giamGia = 0;
        },
      });
    }
    var vConfirm = confirm(`${vAlert}\nBạn có muốn tiếp tục`);
    if (vConfirm) {
      return true;
    } else {
      return false;
    }
  }

  function importValueToFormInput(paramSelector, paramObject) {
    var vInputElements = $(`${paramSelector} input[name]`);
    for (var vInput of vInputElements) {
      vInput.value = paramObject[vInput.name];
    }
  }

  function createOrderObjectAPI(paramObject) {
    "use strict";
    const vBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
    $.ajax({
      url: vBASE_URL,
      type: "POST",
      async: false,
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(paramObject),
      success: function (response) {
        $("#input-order-id").val(response.orderId);
        $("#confirmed-order-modal").modal("show");
      },
      error: (err) => alert("Update không thành công"),
    });
  }

  function testDataInput() {
    $("#input-fullname").val("Khải Nguyên");
    $("#input-email").val("hknguyen@gmail.com");
    $("#input-phone-number").val("0932039908");
    $("#input-address").val("Hồ Chí Minh");
    $("#input-message").val("Full topping");
    $("#input-voucher").val("12354");
    $("#btn-size-small").click();
    $("#btn-type-seafood").click();
    $("#select-drinks").val("COCA");
  }
});
